<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
        function ubah_huruf($string){
            $result_enc = "";
            for ($i = 0; $i <= strlen($string) - 1; $i++) {

                // perubahan dari karakter single ke nilai ASCII, kemudian ditambahkan dengan banyak geseran
                $ascii_value[] = ord($string[$i]) + 1;
            };
    
            for ($i = 0; $i < sizeof($ascii_value); $i++) {
                // perubahan dari karakter nilai ASCII ke single karakter
                $result_enc .= chr($ascii_value[$i]);
            };

            return $result_enc .'<br>';
        };


        echo ubah_huruf('wow'); // xpx
        echo ubah_huruf('developer'); // efwfmpqfs
        echo ubah_huruf('laravel'); // mbsbwfm
        echo ubah_huruf('keren'); // lfsfo
        echo ubah_huruf('semangat'); // tfnbohbu

    ?>
</body>
</html>